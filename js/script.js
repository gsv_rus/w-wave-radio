const buttons = document.querySelectorAll('.desc-top-btn');

// добавить обработчик событий click для каждой кнопки
buttons.forEach(button => {
  button.addEventListener('click', () => {
    // выполнить необходимые действия при клике на кнопку
    if (button.classList.contains('playing')) {
      // кнопка уже проигрывает, нужно остановить
      button.classList.remove('playing');
      // выполнить действия для остановки проигрывания
    } else {
      // кнопка не проигрывает, нужно начать
      button.classList.add('playing');
      // выполнить действия для начала проигрывания
    }
  });
});

const play = document.querySelectorAll('.playing-svg-btn');

// добавить обработчик событий click для каждой кнопки
play.forEach(play => {
  play.addEventListener('click', () => {
    // выполнить необходимые действия при клике на кнопку
    if (play.classList.contains('pausing')) {
      // кнопка уже проигрывает, нужно остановить
      play.classList.remove('pausing');
      // выполнить действия для остановки проигрывания
    } else {
      // кнопка не проигрывает, нужно начать
      play.classList.add('pausing');
      // выполнить действия для начала проигрывания
    }
  });
});

document.querySelectorAll('.blogers-link').forEach(function (tabsBtn) {
  tabsBtn.addEventListener('click', function (e) {
    const path = e.currentTarget.dataset.path;

    document.querySelectorAll('.blogers-link').forEach(function (btn) {
      btn.classList.remove('blogers-link-active')
    }); e.currentTarget.classList.add('blogers-link-active');

    document.querySelectorAll('.guest__right-item').forEach(function (tabsBtn) {
      tabsBtn.classList.remove('guest__right-item-active')
    });
    document.querySelector(`[data-target="${path}"]`).classList.add('guest__right-item-active');
  });
});

document.querySelector('#burger-menu').addEventListener('click', function() {
  document.querySelector('#burger').classList.toggle('is-active'),
  document.querySelector('.header__burger-line').classList.toggle('burger-active')
})


