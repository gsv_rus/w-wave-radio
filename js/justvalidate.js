const validationModal = new JustValidate('#modal-form',
{
  errorFieldCssClass: 'is-invalid',
  errorLabelCssClass: 'is-label-invalid',
  errorLabelStyle: {
    color: '#D52B1E',
  },
  focusInvalidField: true,
},
);

const validation = new JustValidate('#form',
{
  errorFieldCssClass: 'is-invalid',
  errorLabelCssClass: 'is-label-invalid',
  errorLabelStyle: {
    color: '#D52B1E',
  },
  focusInvalidField: true,
},
);

validationModal
.addField('#login', [
  {
    rule: 'required',
    errorMessage: 'Укажите ваш логин',
  },
])
.addField('#password', [
  {
    rule: 'required',
    errorMessage: 'Укажите ваш пароль'
  },
  {
    rule: 'password',
    errorMessage: 'Пароль должен содержать минимум 8 знаков, одну букву и одну цифру'
  },
]);

validation
.addField('#name', [
  {
    rule: 'required',
    errorMessage: 'Укажите ваше имя',
  },
])
.addField('#email', [
  {
    rule: 'required',
    errorMessage: 'Укажите ваш Email',
  },
  {
    rule: 'email',
    errorMessage: 'Укажите корректный Email'
  },
]);