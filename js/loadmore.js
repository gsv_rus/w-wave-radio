// function loadMore() {
//   var dataContainer = document.getElementById("data-container");
//   var dataCounterInput = document.getElementById("data-counter");
//   var dataCounter = parseInt(dataCounterInput.value);

//   // Отображаем следующую порцию данных с плавным появлением
//   for (var i = dataCounter; i < dataCounter + 2; i++) {
//     var paragraph = document.createElement("p");
//     paragraph.innerText = "Загрузить еще" + (i + 1);
//     paragraph.classList.add("fade-in"); // Добавляем класс анимации
//     dataContainer.appendChild(paragraph);
//   }

//   // Увеличиваем счетчик
//   dataCounter += 2;
//   dataCounterInput.value = dataCounter.toString();

//   // Скрываем кнопку "Загрузить еще", если все данные отображены
//   if (dataCounter >= 5) {
//     var loadMoreButton = document.getElementsByClassName("load-more-button")[0];
//     loadMoreButton.style.display = "none";
//   }
// }

document.querySelector('#load-more-btn').addEventListener('click', function() {
  var hiddenPodcasts = document.querySelectorAll('.podcast__block-item');
  hiddenPodcasts.forEach(function(podcast) {
    podcast.classList.add('load-more-active');
  });

  document.querySelector('#load-more-btn').style.display = 'none';
});