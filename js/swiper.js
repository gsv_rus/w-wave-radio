const swiper = new Swiper('.swiper', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  slidesPerView: 3,
  spaceBetween: 30,


  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  breakpoints: {
    250: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    550: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    1500: {
      slidesPerView: 3,
      spaceBetween: 0
    },
    1900: {
      slidesPerView: 4,
      spaceBetween: 30
    },
  }
});